FROM alpine:latest

ARG GIT_USER
ARG GIT_TOKEN

RUN apk add git nodejs npm && \
    git clone https://$GIT_USER:$GIT_TOKEN@gitlab.com/mtu-school/school-api.git && \
    cd school-api && \
    npm install

COPY /services/local.config.ts ./school-api/config/

WORKDIR /school-api

ENTRYPOINT ["tail", "-f", "/dev/null"]